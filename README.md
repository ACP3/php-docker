# ACP3 PHP Docker container repository

## How to build images

* `cd` into the directory with the desired PHP version
* run `docker build -t registry.gitlab.com/acp3/php-docker:<version> .` to build the docker image. `<version>` stands for the PHP version.
* run `docker push registry.gitlab.com/acp3/php-docker:<version>` to push the image into the container registry
